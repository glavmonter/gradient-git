import matplotlib
matplotlib.use("Qt5Agg")

from PyQt5 import QtCore
from PyQt5.QtCore import QTimer, pyqtSignal
from PyQt5.QtWidgets import QSizePolicy
import numpy as np
from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.figure import Figure
from ZoneManager import ZoneManager


class MyMplCanvas(FigureCanvas):
    """Ultimately, this is a QWidget (as well as a FigureCanvasAgg, etc.)."""
    def __init__(self, parent=None, width=5, height=4, dpi=100):
        fig = Figure(figsize=(width, height), dpi=dpi)
        self.axes = fig.add_subplot(111)
        # We want the axes cleared every time plot() is called
        self.axes.hold(False)

        super(MyMplCanvas, self).__init__(fig)
        self.setParent(parent)
        super(MyMplCanvas, self).setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)
        super(MyMplCanvas, self).updateGeometry()


class MplGraph(MyMplCanvas):
    update_signal = pyqtSignal()
    position_changed = pyqtSignal(float, float, float)

    def __init__(self, parent=None):
        super(MplGraph, self).__init__(parent)

        self.x = np.arange(0.0, 220.0, 10)
        self.x_base = self.x
        self.y = np.ndarray(len(self.x))
        self.y.fill(25.0)

        self.lines, = self.axes.plot([], [], 'o-r')
        self.lines.set_xdata(self.x)
        self.lines.set_ydata(self.y)

        self.axes.grid(True)
        self.axes.set_ylim(self.y.min() - 20.0, self.y.max()+20.0)
        self.axes.set_xlim(-10, 230)
        self.axes.xaxis.set_ticks(np.arange(0.0, 230.0, 10.0))

        self.update_timer = QTimer(self)
        self.update_timer.timeout.connect(self.redraw)
        self.update_timer.setInterval(500)
        self.update_timer.setSingleShot(True)

        self._high_t = 1100.0
        self._low_t = 1060.0
        self._melt_t = 1080.0
        self._k1 = 2.0
        self._k2 = 2.0
        self._k3 = 0.16
        self.pos_current = 0.0

        self.axes.axhline(y=self._high_t)
        self.axes.axhline(y=self._low_t)
        self.axes.axhline(y=self._melt_t)

        self._locked = False
        self._start_pos = 0.0
        self.zone_manager = ZoneManager()

    def redraw(self):
        if not self._locked:
            self.calculate_gradient()
            self.update_signal.emit()
        else:
            self.horizontal_move()

        self.lines.set_xdata(self.x)
        self.lines.set_ydata(self.y)
        self.axes.set_ylim(self.y.min() - 20.0, self.y.max() + 20.0)

        self.figure.canvas.draw()
        self.figure.canvas.flush_events()

    def update_graph(self):
        self.lines.set_xdata(self.x)
        self.lines.set_ydata(self.y)
        self.axes.set_ylim(self.y.min() - 20.0, self.y.max() + 20.0)

        self.figure.canvas.draw()
        self.figure.canvas.flush_events()

    def calculate_gradient(self):
        zone_size = 10.0
        for zone in range(len(self.x)):
            t = self._k1*10.0/zone_size*((zone + 1)*zone_size - self.pos_current) + self._melt_t
            if t > self._high_t:
                t = self._high_t

            if t < self._low_t:
                delta = self._k3*10.0/zone_size*((zone + 2)*zone_size - self.pos_current)
                t = self._low_t + delta
            self.y[zone] = t

    def horizontal_move(self):
        delta = self.pos_current - self._start_pos
        self.x = self.x_base + delta
        self.position_changed.emit(self._start_pos, self.pos_current, delta)

    def pos_changed(self, pos):
        self.pos_current = pos/100.0
        self.update_timer.start()

    def value_changed(self, value):
        sender = self.sender()

        self.y[sender.zone_number] = value
        self.redraw()

    def lock_changed(self, lock):
        self._locked = True if lock == QtCore.Qt.Checked else False
        if self._locked:
            self._start_pos = self.pos_current
