#! /usr/bin/env python3
# -*- coding: utf-8 -*-

import json
from PyQt5 import QtCore
from PyQt5.QtWidgets import QApplication, QMainWindow, QDoubleSpinBox, QLabel, QHBoxLayout, QVBoxLayout
from ui_MainWindow import Ui_MainWindow


class ZoneDoubleSpinBox(QDoubleSpinBox):
    def __init__(self, number=0, parent=None):
        super(ZoneDoubleSpinBox, self).__init__(parent)
        self.zone_number = number


class MainWindow(QMainWindow):
    def __init__(self, parent=None):
        super(MainWindow, self).__init__(parent)
        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)
        self.ui.sliderPosition.valueChanged.connect(self.ui.plotGradient.pos_changed)
        self.ui.sliderPosition.valueChanged.connect(self.pos_changed)
        self.ui.plotGradient.update_signal.connect(self.plot_update)
        self.ui.checkLock.stateChanged.connect(self.ui.plotGradient.lock_changed)
        self.ui.btnSave.clicked.connect(self.save)
        self.ui.btnLoad.clicked.connect(self.load)
        self.ui.plotGradient.position_changed.connect(self.show_positions)

        self.zones = []
        zone_index = 0
        zones_layout = QVBoxLayout(self.ui.Zones)
        for controller in range(3):
            controller_layout = QHBoxLayout()
            zones_layout.addLayout(controller_layout)

            for z in range(8):
                layout = QHBoxLayout()
                label = QLabel('{:02d}:'.format(zone_index), self.ui.Zones)
                label.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter)
                zone = ZoneDoubleSpinBox(zone_index, self.ui.Zones)
                zone.setRange(25.0, 1150.0)
                zone.setValue(25.0)
                zone.valueChanged.connect(self.ui.plotGradient.value_changed)
                self.zones.append(zone)

                layout.addWidget(label)
                layout.addWidget(zone)
                controller_layout.addLayout(layout)
                zone_index += 1

        self.zones[22].setEnabled(False)
        self.zones[23].setEnabled(False)
        self.zones.remove(self.zones[23])
        self.zones.remove(self.zones[22])

        self.show_positions(0.0, 0.0, 0.0)

    def show_positions(self, start, current, delta):
        self.ui.labelPosition_2.setText('Старт: {:.2f} мм. Текущее: {:.2f} мм. Смещение: {:.2f} мм.'
                                        .format(start, current, delta))

    def pos_changed(self, pos):
        self.ui.labelPosition.setText('Позиция: {:.2f} мм'.format(pos/100.0))

    def plot_update(self):
        for zone in self.zones:
            zone.setValue(self.ui.plotGradient.y[zone.zone_number])

    def save(self):
        l = list(map(lambda x: x.value(), self.zones))
        f = open('data.json', 'w')
        f.write(json.dumps({'T': l, 'Position': self.ui.sliderPosition.value()}))
        f.close()

    def load(self):
        try:
            f = open('data.json')
            d = json.load(f)
            f.close()

            for z in self.zones:
                t = d['T'][z.zone_number]
                z.blockSignals(True)
                z.setValue(t)
                self.ui.plotGradient.y[z.zone_number] = t
                z.blockSignals(False)

            self.ui.sliderPosition.blockSignals(True)
            self.ui.sliderPosition.setValue(d['Position'])
            self.pos_changed(self.ui.sliderPosition.value())
            self.ui.plotGradient.pos_current = d['Position']/100.0
            self.ui.sliderPosition.blockSignals(False)

            self.ui.checkLock.setChecked(True)
            self.ui.plotGradient.redraw()
        except IOError:
            pass


if __name__ == '__main__':
    import sys
    app = QApplication(sys.argv)
    w = MainWindow()
    w.show()
    sys.exit(app.exec_())
